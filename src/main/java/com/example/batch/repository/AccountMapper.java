package com.example.batch.repository;

import java.util.List;

import com.example.batch.domain.Account;

public interface AccountMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(Account record);

    int insertSelective(Account record);
    
    //非会员检索的集合，判断member_status值，为“0”未验证 或 为“1”验证中
    List<Account> selectList();
    
    //非会员检索的集合，判断member_status值，为“2”非会员，但有优惠券
    List<Account> selectUnmembeCouponList();
    
    Account selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);
}