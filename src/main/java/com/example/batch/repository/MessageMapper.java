package com.example.batch.repository;

import java.util.List;

import com.example.batch.domain.Message;

public interface MessageMapper {
    
	int deleteByPrimaryKey(Integer messageId);

   int insert(Message record);

    int insertSelective(Message record);
    
    List<Message> selectMessage24(java.sql.Timestamp time);

    Message selectByPrimaryKey(Integer messageId);

    int updateByPrimaryKeySelective(Message record);

    int updateByPrimaryKey(Message record);
}