package com.example.batch.repository;


import java.util.List;

import com.example.batch.domain.Share;

public interface ShareMapper {
    int deleteByPrimaryKey(Integer shareId);

    int insert(Share record);
    

    int insertSelective(Share record);
  
    //  ステータスシェア開始（予約リクエスト承認期限切れ，测试暂定状态值为“ 1 ” ,>= 参数时间
    List<Share> selectBespeakTime(java.sql.Timestamp day1);
    
    //  レビュー期限締切り，終了時間14天之后，返回结果list
    List<Share> selectEndTime(java.sql.Timestamp day1);
   
    //ステータスシェア開始（ドライブ中），测试暂定状态值为“ 2 ” ,<= 参数时间
    List<Share> selectScarborrow(java.sql.Date time);
   
    //ステータスシェア開始（シェア終了） ，测试暂定状态值为“ 3 ” ,>= 参数时间
    List<Share> selectOcarborrow(java.sql.Date time);
    
    
    //  ステータスシェア開始（問い合わせ期限切れ），测试暂定状态值为“ 1 ” ,>= 参数时间
    List<Share> selectConsultMessage(java.sql.Timestamp day1);
    
    
    //根据share_id, シェアID查出预约开始时间，来进行对比
    Share selectDataTime (Integer shareId);
    
  //予約リマインド、予約時間 - システム日付 =  1日,システム時刻=18：00/8：00  にリマインド通知。 测试时间暂定8 or 14
    List<Share> selectListAMPM();
    
    //「貸出確定」ユーザーへ予約開始時間の1時間前にプッシュ				  ，状态值为“ 1 ”的结果集 ,测试暂定为1
	 /*  条件
	  	 	シェア
	    	WHERE  状態区分 = ドライブ予定（暂定为1）
	　		AND T1.予約時間 - システム時刻 = 1時間の場合 */
    List<Share> selectListStart1H();
    
    //「返却時間の2時間前に返却と返却前の給油等のリマインド				  ，状态值为“ 1 ”的结果集 ,测试暂定为1
   /*　シェア 
    WHERE 状態区分 = ドライブ中
　AND 終了時間 - システム時刻 = 2時間の場合
 【アプリ通知非同期処理】設計書を参照して通知する。*/
    List<Share> selectListEnd2H();
    
    
  //「レビュー登録待ち」のステータスのままの7日後に、会員へ通知				  ，状态值为“ 1 ”的结果集 ,测试暂定为1
  /* 　シェア 
    WHERE 状態区分 = レビュー登録待ち
　AND システム日付 - 終了時間 >= 7日の の場合*/

    List<Share> selectListReview7D();
    
    Share selectByPrimaryKey(Share record);

    int updateByPrimaryKeySelective(Share record);

    int updateByPrimaryKey(Share record);
}