package com.example.batch.repository;

import java.util.List;

import com.example.batch.domain.Review;

public interface ReviewMapper {
    int deleteByPrimaryKey(Integer reviewId);

    int insert(Review record);

    int insertSelective(Review record);
	/**
	 * @Description 检查评论，双方都有评价，并在2天之内，返回结果list 状态分区1 -> 2 双方公开评论
	 * @param Review insert_time 目前暂定此时间
	 */
    List<Review> selectInsertAll(java.sql.Timestamp time);
    
	/**
	 * @Description 检查评论，评价时间2天以后，返回结果list 状态分区1 -> 2 双方公开评论
	 * @param Review
	 */
    List<Review> selectInsertTimee2(java.sql.Timestamp time);
    
    
    Review selectByPrimaryKey(Integer reviewId);

    int updateByPrimaryKeySelective(Review record);

    int updateByPrimaryKey(Review record);
}