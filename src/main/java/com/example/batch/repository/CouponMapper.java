package com.example.batch.repository;

import java.sql.Timestamp;
import java.util.List;

import com.example.batch.domain.Coupon;

public interface CouponMapper {
    int deleteByPrimaryKey(Integer couponId);

    int insert(Coupon record);

    int insertSelective(Coupon record);

    /*
     * 非会员优惠券, 当前系统时间在其有效期内 ， インセンティブタイプ类型为1
     * 参数，当前系统时间
     * return 有效期内list
     */
    List<Coupon> selectUnmemberList(Timestamp nowTime);
   
    //这个测试用
    List<Coupon> selectUnList();
    
    Coupon selectByPrimaryKey(Integer couponId);

    int updateByPrimaryKeySelective(Coupon record);

    int updateByPrimaryKey(Coupon record);
}