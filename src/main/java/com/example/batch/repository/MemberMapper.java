package com.example.batch.repository;

import java.util.List;

import com.example.batch.domain.Member;

public interface MemberMapper {
    int deleteByPrimaryKey(Member key);

    int insert(Member record);

    int insertSelective(Member record);
    //会员入会后，1个月内，没有共享记录，结果集
    List<Member> selectUnShare();

    Member selectByPrimaryKey(Member key);

    int updateByPrimaryKeySelective(Member record);

    int updateByPrimaryKey(Member record);
}