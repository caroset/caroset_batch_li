package com.example.batch.repository;

import java.util.List;

import com.example.batch.domain.Loan;
import com.example.batch.domain.LoanAdd;
import com.example.batch.domain.Share;

public interface LoanMapper {
    int deleteByPrimaryKey(Integer key);

    int insert(Loan record);

    int insertSelective(Loan record);
    //  ステータスシェア開始（予約リクエスト承認期限切れ，测试暂定状态值为“ 1 ” ,>= 参数时间 to  貸出し
    List<Loan> selectBespeakTime(java.sql.Timestamp day1);
    
    //  レビュー期限締切り，終了時間14天之后，返回结果list to  貸出し
    List<Loan> selectEndTime(java.sql.Timestamp day1);
    
    //ステータスシェア開始（ドライブ中），测试暂定状态值为“ 2 ” ,<= 参数时间  to  貸出し
    List<Loan> selectScarborrow(java.sql.Date time);
    
    //ステータスシェア開始（シェア終了），测试暂定状态值为“ 1 ” ,>= 参数时间  to  貸出し
    List<Loan> selectOcarborrow(java.sql.Date time);
    
    // ステータスシェア開始（問い合わせ期限切れ），测试暂定状态值为“ 1 ” ,>= 参数时间 to  貸出し
    List<Loan> selectConsultMessage(java.sql.Timestamp day1);
   
    //貸出前リマインド、予約時間 - システム日付 =  1日,システム時刻=18：00/8：00  にリマインド通知。 测试时间暂定8 or 15
    List<LoanAdd> LselectListAMPM();
    // 予約リクエスト返信リマインド ，一下条件
   /*　	シェア
   　		シェア T1
   　		INNER JOIN 貸出し T2
   　		WHERE T2.シェアID= T1.シェアID
   　		T1.状態区分＝予約リクエスト
   　		T2.リクエスト回答時刻 is null
       			(システム時刻 - T2.リクエスト受信時刻 >= 21 時間の場合 )改为下句
       	T2.リクエスト受信時刻 <= システム時刻 -21 時間の場合 */
    List<Loan> selectUnllEndData();
    
    //「貸出確定」ユーザーへ予約開始時間の1時間前にプッシュ				  ，状态值为“ 1 ”的结果集 ,测试暂定为1
	 /*  条件
	  	 	シェア
	    	WHERE  状態区分 = ドライブ予定（暂定为1）
	　		AND T1.予約時間 - システム時刻 = 1時間の場合 */
    List<Loan> LselectListStart1H();
    
    //「返却時間の2時間前に返却と返却前の給油等のリマインド				  ，状态值为“ 1 ”的结果集 ,测试暂定为1
   /*　シェア 
    WHERE 状態区分 = ドライブ中
　AND 終了時間 - システム時刻 = 2時間の場合
 【アプリ通知非同期処理】設計書を参照して通知する。*/
    List<Loan> LselectListEnd2H();
    
    //「レビュー登録待ち」のステータスのままの7日後に、会員へ通知				  ，状态值为“ 1 ”的结果集 ,测试暂定为1
    /* 貸出し
　シェア T1
　INNER JOIN 貸出し T2
    WHERE T2.シェアID= T1.シェアID
　AND T2.状態区分 = レビュー登録待ち
　AND システム日付 - T1.終了時間 >= 7日の の場合*/
      List<Loan> LselectListReview7D();
    
    Loan selectByPrimaryKey(Integer key);

    int updateByPrimaryKeySelective(Loan record);

    int updateByPrimaryKey(Loan record);
}