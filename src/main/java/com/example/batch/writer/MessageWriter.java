package com.example.batch.writer;
import java.util.List;

import org.springframework.batch.item.support.AbstractItemStreamItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.Message;
import com.example.batch.tasklet.MessageService;


public class MessageWriter extends AbstractItemStreamItemWriter<Message>{
	
	@Autowired
	MessageService messageService;
	
	 @Override
	    public void write(List<? extends Message> list) throws Exception {

	        list.stream()
	                .filter(t -> t != null)
	                .forEach(t -> {
	                	messageService.insert(t);    
	                
	                });
	    }


}
