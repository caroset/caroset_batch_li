package com.example.batch.BespeakTime1H;

import org.springframework.batch.item.ItemProcessor;

import com.example.batch.domain.Message;
import com.example.batch.domain.Share;
/**
*	65,66处理貸出BATCH
*	Processor
* 	予約関連
*  シェア前チェック促進
*  「貸出確定」ユーザーへ予約開始時間の1時間前にプッシュ
*  確認事項トップ
*  通知専用
*/
public class LBespeakTime1HProcessor  implements ItemProcessor<Share, Message> {
	
    @Override
    public Message process(Share share) throws Exception {
	    final Message message = new Message();
    	// 判断参数集合是否为空
    	if (share == null) {
    		 //不符合条件的，舍弃返回null
    		 return null;
/*//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");*/
        }else {
        	 message.setMemberId(share.getMemberId());
        	 //借出id
             message.setLoanId(share.getCarId());
             //发送消息内容
             message.setContent("S预约时间1小时内，发message");
          //返回message
         return message;
        	
        }
    }
    
}