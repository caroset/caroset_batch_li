package com.example.batch.membernew1m;

import java.util.Calendar;
import java.util.Date;
import org.springframework.batch.item.ItemProcessor;
import com.example.batch.domain.Member;
import com.example.batch.domain.Message;
/**
 *	前借促進 BATCH
 *  "入会一定期間（１カ月）後に、一度もシェアしてない会員に、 前借での予約を促進"
 *  "【ロジック判断】 ベース： 会員全員×入会済×入会日×シェア全件チェック 詳細条件： 入会日，シェアレコードなし，規定日数 "
 *  通知
 */
public class MemberNew1MProcessor implements ItemProcessor<Member, Message> {
	
	//参数时间，对其进行增减操作，
	private Date getTime(Date tt) {
		  Calendar cc = Calendar.getInstance();
		  cc.setTime(tt);
		  //参数时间+1个月
		  cc.add(Calendar.MONTH,+1);
		  //返回处理后时间
		  return cc.getTime();
	}
	
    @Override
    public Message process(Member member) throws Exception {
    	// 判断参数集合是否为空
    	if (member == null) {
//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");
        }
        
    	//取得当前系统时间
    	Date nowTime = new Date(); 
	     //取得Member中取 入会日
	    Date memberDate = member.getBirthday();
	    Date afterDate = getTime(memberDate);
	     //声明message
	     final Message message = new Message();
	      // 判断目前系统时间是否在，入会日1个月之后
	     if( afterDate.before(nowTime)) {
	    	//在条件范围内，则给message赋值，之后根据message设计再进行修改
	    	 	
	    	 //这里id反写，等message相关内容建好后更改
	    	    	 
	    	 //会员id
	        	 message.setMemberId(member.getMemberId());
	        	 //借出id
	             message.setLoanId(member.getUserId());
	             //发送消息内容
	             message.setContent("入会都一个月了，咋还不共享车子呢！");
	          //返回message
	         return message;
	     }else {
	    	//不符合条件的，舍弃返回null
	    	 return null;
	     }
	     	
       
    }
    
}