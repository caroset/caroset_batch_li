package com.example.batch.chunk;

import com.example.batch.domain.Loan;
import com.example.batch.domain.Message;
//import com.example.batch.repository.ShareMapper;
import java.sql.Timestamp;
import java.util.Calendar;
import org.springframework.batch.item.ItemProcessor;
//import org.springframework.beans.factory.annotation.Autowired;
/**
 *	60，处理貸出BATCH
 *	Processor
 * 	貸出関連
 *  予約リクエスト返信リマインド  
 *  予約リクエストを受信して期限まで3時間切ってリアクションをしていない際にプッシュ
 *  通知専用
 */
public class BonusCalcProcessor implements ItemProcessor<Loan, Message> {

    @Override
    public Message process(Loan loan) throws Exception {
    	 final Message message = new Message();
    	// 判断参数集合是否为空
    	if (loan == null) {
          //不符合条件的，舍弃返回null
	    	 return null;
        }else {
        	
//在条件范围内，则给message赋值，之后根据message设计再进行修改
    	 	//会员id
        	 message.setMemberId(loan.getMemberId());
        	 //借出id
             message.setLoanId(loan.getLoanId());
             //发送消息内容
             message.setContent("ビジターサイド、ホームサイド双方に給油のリマインドが必要");
          //返回message
         return message;
        }
       
    }
    
}
