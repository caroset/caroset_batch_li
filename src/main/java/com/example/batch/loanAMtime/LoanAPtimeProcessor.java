package com.example.batch.loanAMtime;

	import com.example.batch.domain.Message;
	import com.example.batch.domain.Share;
	import java.sql.Timestamp;
	import java.util.Calendar;
	import org.springframework.batch.item.ItemProcessor;

	/**
	 *	61,63处理貸出BATCH
	 *	Processor
	 * 	貸出関連
	 *  貸出前リマインド
	 *  予約時間がAM、PMの場合は、予約日前日の18:00/8:00にリマインド通知
	 *  通知専用
	 */
	public class LoanAPtimeProcessor implements ItemProcessor<Share, Message> {
		
		//参数时间，取得时间小时数
		private int hourTime(Timestamp time) {
			  Calendar cc = Calendar.getInstance();
			  cc.setTime(time);
			  //取参数时间小时数
			int  hour = cc.get(Calendar.HOUR_OF_DAY);
			return hour;
		}
		
	    @Override
	    public Message process(Share share) throws Exception {
	    	 //声明message
		     final Message message = new Message();
		     Timestamp nowTime = new Timestamp(System.currentTimeMillis());
		     int sysHour = hourTime(nowTime);
		     int bespeakTimeHour = 0; 
	    	// 判断参数集合是否为空
	    	if (share == null) {
		     	//返回message
		        return null;
/*//异常，为空抛出为空信息
	            throw new IllegalArgumentException("loan must not be null.");*/
	        }else {
	        	bespeakTimeHour = hourTime(share.getBespeakTime()); 
	        }
	    	
	    	if(sysHour == 8 || bespeakTimeHour <= 12) {
	    		 //在AM，message赋值，之后根据message设计再进行修改
	    	 	//会员id
	        	 message.setMemberId(share.getMemberId());
	        	 //借出id； 目前把车id存到借出id中，等message建好后修改
	             message.setLoanId(share.getCarId());
	             //发送消息内容
	             message.setContent("明天上午您有车辆预约");
	           //返回message
	 	        return message;
	    		
	    		}else if(sysHour == 15 || bespeakTimeHour > 12) {
	    			 //在AM，message赋值，之后根据message设计再进行修改
		    	 	//会员id
		        	 message.setMemberId(share.getMemberId());
		        	 //借出id； 目前把车id存到借出id中，等message建好后修改
		             message.setLoanId(share.getCarId());
		             //发送消息内容
		             message.setContent("明天下午您有车辆预约");
		           //返回message
		 	        return message;
	    		}else {
	    		
	    		return null;
	    	}

	    }
}
