package com.example.batch.loanAMtime;

import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.batch.item.ItemProcessor;

import com.example.batch.domain.Loan;
import com.example.batch.domain.LoanAdd;
import com.example.batch.domain.Message;
import com.example.batch.domain.Share;

/**
 *	62,64处理貸出BATCH
 *	Processor
 * 	貸出関連
 *  予約リクエスト返信リマインド  
 *  予約時間がAM、PMの場合は、予約日前日の18:00/8:00にリマインド通知
 *  通知専用
 */
public class CarAPtimeProcessor implements ItemProcessor<LoanAdd, Message> {
	
	//参数时间，取得时间小时数
	private int hourTime(Timestamp time) {
		  Calendar cc = Calendar.getInstance();
		  cc.setTime(time);
		  //取参数时间小时数
		int  hour = cc.get(Calendar.HOUR_OF_DAY);
		return hour;
	}
    @Override
    public Message process(LoanAdd loanAdd) throws Exception {
   	 
    	//声明message
	     final Message message = new Message();
	     Timestamp nowTime = new Timestamp(System.currentTimeMillis());
	     int sysHour = hourTime(nowTime);
	     int bespeakTimeHour = 0; 
    	
    	// 判断参数集合是否为空
    	if (loanAdd == null) {
    		//返回message
	        return null;
/*//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");*/
        }else {
        	//暂借loan表中的request_recept_time时间字段
        	bespeakTimeHour = hourTime(loanAdd.getBespeakTime()); 
        }
    	
    	if(sysHour == 8 || bespeakTimeHour <= 12) {
    		 //在AM，message赋值，之后根据message设计再进行修改
    	 	//会员id
        	 message.setMemberId(loanAdd.getMemberId());
        	 //借出id； 目前把车id存到借出id中，等message建好后修改
             message.setLoanId(loanAdd.getLoanId());
             //发送消息内容
             message.setContent("明天上午您有车辆要借出");
           //返回message
 	        return message;
    		
    		}else if(sysHour == 15 || bespeakTimeHour > 12) {
    			 //在AM，message赋值，之后根据message设计再进行修改
	    	 	//会员id
	        	 message.setMemberId(loanAdd.getMemberId());
	        	 //借出id； 目前把车id存到借出id中，等message建好后修改
	             message.setLoanId(loanAdd.getLoanId());
	             //发送消息内容
	             message.setContent("明天下午您有车辆要借出");
	           //返回message
	 	        return message;
    		}else {
    		
    		return null;
    	}
    }
}
