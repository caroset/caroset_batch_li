package com.example.batch.Review7D;

import org.springframework.batch.item.ItemProcessor;

import com.example.batch.domain.Loan;
import com.example.batch.domain.Message;
/**
*	72,81处理 貸出予約関連  BATCH
*	BATCH
*  レビュー登録期限予告
*  「レビュー登録待ち」のステータスのままの7日後に、会員へ通知
*  貸出,予約詳細
*  通知専用
*/
public class CReview7DProcessor  implements ItemProcessor<Loan, Message> {
    @Override
    public Message process(Loan loan) throws Exception {
    	 //声明message
        final Message message = new Message();
    	// 判断参数集合是否为空
    	if (loan == null) {
	    	//不符合条件的，舍弃返回null
	    	 return null;
/*//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");*/
        }else {
        	//在条件范围内，则给message赋值，之后根据message设计再进行修改	    	
       	 //这里id反写，等message相关内容建好后更改

   	 	//会员id
       	 message.setMemberId(loan.getMemberId());
       	 //借出id
            message.setLoanId(loan.getLoanId());
            
            //目前暂定发送时间为当前系统时间
           // message.setSendMailDate(nowTime);
            //发送消息内容
            message.setContent("「レビュー登録待ち」のステータスのままの7日後に、会員へ通知");
         //返回message
        return message;
        	
        }
       
    }
    
}
