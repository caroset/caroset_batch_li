package com.example.batch.Review7D;

import org.springframework.batch.item.ItemProcessor;

import com.example.batch.domain.Message;
import com.example.batch.domain.Share;
/**
*	72,81处理 貸出予約関連  BATCH
*	BATCH
*  レビュー登録期限予告
*  「レビュー登録待ち」のステータスのままの7日後に、会員へ通知
*  貸出,予約詳細
*  通知専用
*/
public class LReview7DProcessor  implements ItemProcessor<Share, Message> {
	
    @Override
    public Message process(Share share) throws Exception {
    	 //声明message
	       final Message message = new Message();
    	// 判断参数集合是否为空
    	if (share == null) {
    		//不符合条件的，舍弃返回null
	    	 return null;
/*    	//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");*/
        }else{
        	//在条件范围内，则给message赋值，之后根据message设计再进行修改	
    	 	//会员id
        	 message.setMemberId(share.getMemberId());
        	 //借出id
             message.setLoanId(share.getCarId());
             
             //目前暂定发送时间为当前系统时间
            // message.setSendMailDate(nowTime);
             //发送消息内容
             message.setContent("S「レビュー登録待ち」のステータスのままの7日後に、会員へ通知");
          //返回message
         return message;
        }
               
    }
    
}
