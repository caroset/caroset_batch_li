package com.example.batch.domain;

import java.sql.Timestamp;
import java.util.Date;

public class LoanAdd {
	//key
	
    private Integer loanId;

    private Integer shareId;

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getShareId() {
        return shareId;
    }

    public void setShareId(Integer shareId) {
        this.shareId = shareId;
    }
	
	//key
    private Integer memberId;

    private Timestamp requestReceptTime;

    private Date endDate;

    private Integer stateDivision;

    private String cancelReason;

    private String loanMemo;

    private String actualFlg;

    private String insertUser;

    private Timestamp insertTime;

    private String updateUser;

    private Short version;
    
    private Timestamp bespeakTime;
    
    public Timestamp getBespeakTime() {
        return bespeakTime;
    }

    public void setBespeakTime(Timestamp bespeakTime) {
        this.bespeakTime = bespeakTime;
    }
    

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Timestamp getRequestReceptTime() {
        return requestReceptTime;
    }

    public void setRequestReceptTime(Timestamp requestReceptTime) {
        this.requestReceptTime = requestReceptTime;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStateDivision() {
        return stateDivision;
    }

    public void setStateDivision(Integer stateDivision) {
        this.stateDivision = stateDivision;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason == null ? null : cancelReason.trim();
    }

    public String getLoanMemo() {
        return loanMemo;
    }

    public void setLoanMemo(String loanMemo) {
        this.loanMemo = loanMemo == null ? null : loanMemo.trim();
    }

    public String getActualFlg() {
        return actualFlg;
    }

    public void setActualFlg(String actualFlg) {
        this.actualFlg = actualFlg == null ? null : actualFlg.trim();
    }

    public String getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(String insertUser) {
        this.insertUser = insertUser == null ? null : insertUser.trim();
    }

    public Timestamp getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Timestamp insertTime) {
        this.insertTime = insertTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }
}