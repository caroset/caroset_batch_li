package com.example.batch.domain;

import java.util.Date;

public class Coupon {
    private Integer couponId;

    private String couponName;

    private String incentiveType;

    private Integer incentivesIdFk;

    private Integer giveIdFk;

    private Date startTime;

    private Date endTime;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName == null ? null : couponName.trim();
    }

    public String getIncentiveType() {
        return incentiveType;
    }

    public void setIncentiveType(String incentiveType) {
        this.incentiveType = incentiveType == null ? null : incentiveType.trim();
    }

    public Integer getIncentivesIdFk() {
        return incentivesIdFk;
    }

    public void setIncentivesIdFk(Integer incentivesIdFk) {
        this.incentivesIdFk = incentivesIdFk;
    }

    public Integer getGiveIdFk() {
        return giveIdFk;
    }

    public void setGiveIdFk(Integer giveIdFk) {
        this.giveIdFk = giveIdFk;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}