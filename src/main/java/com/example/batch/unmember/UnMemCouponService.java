package com.example.batch.unmember;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.batch.domain.Coupon;
import com.example.batch.repository.CouponMapper;

public class UnMemCouponService {
	
	@Autowired
	CouponMapper couponMapper;
	
	//非会员优惠券, 当前系统时间在其有效期内 ， インセンティブタイプ类型为1
	public List<Coupon> couponUnmemberList(){
		 Timestamp nowTime = new Timestamp(System.currentTimeMillis());
		return couponMapper.selectUnmemberList(nowTime);
	}

}
