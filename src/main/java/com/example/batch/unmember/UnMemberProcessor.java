package com.example.batch.unmember;

import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.batch.item.ItemProcessor;
import com.example.batch.domain.Account;
import com.example.batch.domain.Message;


public class UnMemberProcessor  implements ItemProcessor<Account, Message> {
		
	//参数最後の登録日時，是否1个月10天没登录，即>=当前系统时间
	private boolean boTime(Timestamp tt) {
		 Calendar cc = Calendar.getInstance();
		  cc.setTime(tt);
		  //参数时间+1个月
		 cc.add(Calendar.MONTH, +1);
		  //参数时间+10天
		 cc.add(Calendar.DAY_OF_MONTH, +10);
		  //取得当前系统时间
		  Timestamp nowTime = new Timestamp(System.currentTimeMillis());	  
		 //当最後の登録日時,调整后时间在现在时间之后，返回true 
		  Timestamp dd = new Timestamp(cc.getTimeInMillis());
		  
		if(dd.before(nowTime)) {
			//上午返回 true
			return true;
		}else {
			//下午返回false
			return false;
		}
	}
    @Override
    public Message process(Account account) throws Exception {
    	// 判断参数集合是否为空
    	if (account == null) {
//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");
        }

	     //取得account对象中最後の登録日時
	    Timestamp endTime = account.getInsertTime();  
	     //声明message
	       final Message message = new Message();
	      // 判断预约时间AM，还是PM
	     if( boTime(endTime)) {
	    	 //在AM，message赋值，之后根据message设计再进行修改
	    		    	 
	    	 //这里id反写，等message相关内容建好后更改
	    	 	    	 
	    	 	//userid，    收信人：未认证者
	        	 message.setMemberId(account.getUserId());
	        	 
	        	//邮箱设定
	        	 //message.setInsertUser(account11.getMail());
	        
	             //发送消息内容
	             message.setContent("未验证！");
	             
	 	     	//返回message
		 	        return message;
		     }else {
		    	 return null;
		     }
	    }
}
