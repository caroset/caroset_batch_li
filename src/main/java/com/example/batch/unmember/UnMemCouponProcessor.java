package com.example.batch.unmember;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.batch.item.ItemProcessor;
import com.example.batch.domain.Account;
import com.example.batch.domain.Message;

public class UnMemCouponProcessor   implements ItemProcessor<Account, Message> {
			
	//参数,最後の登録日時,与当前时间，相差是否是3天或3天倍数
	private boolean boTime(Timestamp tt) {
		//取最后登录时间或入会时间
		Calendar cc = Calendar.getInstance();
		  cc.setTime(tt);
		  //设定小时、分钟为0
		  cc.add(Calendar.HOUR_OF_DAY, 0);
		  cc.add(Calendar.MILLISECOND, 0);
		  Date date1 = new Date(cc.getTimeInMillis());
		  //取当前系统时间
		  Timestamp nowTime = new Timestamp(System.currentTimeMillis());
		  cc.setTime(nowTime);
		  //设定小时、分钟为0
		  cc.add(Calendar.HOUR_OF_DAY, 0);
		  cc.add(Calendar.MILLISECOND, 0);
		  Date date2 = new Date(cc.getTimeInMillis());
		  //两者相差天数，取整数
		  int differDay = new Long( (date2.getTime()-date1.getTime())/(24*3600*1000)).intValue();
		  //判断是否是3天倍数，是true，否false
		  if(differDay%3 == 0) {
			  return true;
		  }else {
			  return false;
		  }		
		}
	
    @Override
    public Message process(Account account) throws Exception {
    	// 判断参数集合是否为空
    	if (account == null) {
//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");
        }

	     //取得account对象中最後の登録日時 或 入会时间
	    Timestamp endTime = account.getInsertTime();  
	     //声明message
	       final Message message = new Message();
	      // 判断时间相差天数 是否3天或3天倍数
	     if( boTime(endTime)) {
	    	 //3天或3天倍数，发送消息给非会员用户
	    	 //在AM，message赋值，之后根据message设计再进行修改
	    		    	 
	    	 //这里id反写，等message相关内容建好后更改
	    	 	    	 
	    	 	//userid，    收信人：未认证者
	        	 message.setMemberId(account.getUserId());
	        	 
	        	//邮箱设定
	        	 //message.setSendMai(account.getMail());
	        
	             //发送消息内容
	             message.setContent("3天了，该发消息了");
	             
	 	     	//返回message
		 	        return message;
		     }else {
		    	//不是3天或3天倍数，舍弃
		    	 return null;
		     }
	    }
}
