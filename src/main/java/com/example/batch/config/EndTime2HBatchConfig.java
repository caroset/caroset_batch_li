package com.example.batch.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;

import com.example.batch.EndTime2H.CEndTime2HProcessor;
import com.example.batch.EndTime2H.LEndTime2HProcessor;
import com.example.batch.domain.Loan;
import com.example.batch.domain.Message;
import com.example.batch.domain.Share;

/**
*	65,66处理貸出BATCH
*	BATCH
* 	貸出関連
*  返却受渡リマインド
*  シェア終了時間と現在時間の時間差の確認
*  確認事項トップ
*  通知専用
*/

//@Configuration
//@EnableBatchProcessing
public class EndTime2HBatchConfig {
	 private JobBuilderFactory jobBuilderFactory;

	    private StepBuilderFactory stepBuilderFactory;

	    private SqlSessionFactory sqlSessionFactory;

	    @Autowired
	    public EndTime2HBatchConfig(JobBuilderFactory jobBuilderFactory,
	                       StepBuilderFactory stepBuilderFactory,
	                       SqlSessionFactory sqlSessionFactory) {
	        this.jobBuilderFactory = jobBuilderFactory;
	        this.stepBuilderFactory = stepBuilderFactory;
	        this.sqlSessionFactory = sqlSessionFactory;
	    }
	//读入数据 收件人 预约者
	 @Bean
	    public MyBatisCursorItemReader<Share> reader() {
	        final MyBatisCursorItemReader<Share> reader = new MyBatisCursorItemReader<>();
	        reader.setSqlSessionFactory(sqlSessionFactory);
	        //シェア，share表中，状态值为“ 1 ”的结果集 ,测试暂定为1
	        reader.setQueryId("com.example.batch.repository.ShareMapper.selectListEnd2H");
	        return reader;
	    }
	//读入数据 收件人 车主 
	 @Bean
	    public MyBatisCursorItemReader<Loan> memberReader() {
	        final MyBatisCursorItemReader<Loan> reader = new MyBatisCursorItemReader<>();
	        reader.setSqlSessionFactory(sqlSessionFactory);
	      //シェア，share表中，状态值为“ 1 ”的结果集 ,测试暂定为1
	        reader.setQueryId("com.example.batch.repository.LoanMapper.LselectListEnd2H");
	        return reader;
	    }


	 	//处理数据 收件人 预约者
	    @Bean
	    public LEndTime2HProcessor processor() {
	        return new LEndTime2HProcessor();
	    }
	 	//处理数据 收件人 车主
	    @Bean
	    public CEndTime2HProcessor carprocessor() {
	        return new CEndTime2HProcessor();
	    }

	    
	    //写入数据  收件人 预约者
	    @Bean
	    public MyBatisBatchItemWriter<Message> writer() {
	        final MyBatisBatchItemWriter<Message> writer = new MyBatisBatchItemWriter<>();
	        writer.setSqlSessionFactory(sqlSessionFactory);
	      //message表，向符合条件的用户双方发送message，向message表中插入要发送的message
	        writer.setStatementId("com.example.batch.repository.MessageMapper.insert");
	        return writer;
	    }

	    
	    //写入数据  收件人 车主
	    @Bean
	    public MyBatisBatchItemWriter<Message> memberWriter() {
	        final MyBatisBatchItemWriter<Message> writer = new MyBatisBatchItemWriter<>();
	        writer.setSqlSessionFactory(sqlSessionFactory);
	      //message表，向符合条件的用户双方发送message，向message表中插入要发送的message
	        writer.setStatementId("com.example.batch.repository.MessageMapper.insert");
	        return writer;
	    }

	//监听
	    @Bean
	    public JobExecutionListener listener() {
	        return new JobExecutionListener() {
	        	private long startTime;
	            private long endTime;
	            @Override
	            public void beforeJob(JobExecution jobExecution) {
	            	startTime = System.currentTimeMillis();
	                System.out.println("before job");
	            }

	            @Override
	            public void afterJob(JobExecution jobExecution) {
	            	endTime = System.currentTimeMillis();
	                System.out.println("after job");
	                System.out.println("耗时："+(endTime-startTime)+"ms");
	            }
	        };
	    }
	  //步骤：给预约者发message
	    @Bean
	    public Step readShareAndWriteMessage() {
	        return stepBuilderFactory
	                .get("readShareAndWriteMessage")
	                .<Share, Message>chunk(10)
	                .reader(reader())
	                .processor(processor())
	                .writer(writer())
	                .build();
	    }
	//步骤：给车主发message
	   @Bean
	    public Step readmemberWrite() {
	        return stepBuilderFactory
	                .get("readmemberWrite")
	                .<Loan, Message>chunk(10)
	                .reader(memberReader())
	                .processor(carprocessor())
	                .writer(memberWriter())
	                .build();
	    }
	//Batch   JOB
	    @Bean
	    public Job singleJob() {
	        return jobBuilderFactory
	                .get("EndTime2H")
	                .incrementer(new RunIdIncrementer())
	                .listener(listener())
	                .flow(readShareAndWriteMessage())
	                .next(readmemberWrite())
	                .end()
	                .build();
	    }

	}