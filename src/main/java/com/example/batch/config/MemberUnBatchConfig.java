package com.example.batch.config;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import com.example.batch.domain.Account;
import com.example.batch.domain.Message;
import com.example.batch.domain.Share;
import com.example.batch.unmember.UnMemCouponProcessor;
import com.example.batch.unmember.UnMemberProcessor;
import com.example.batch.writer.MessageWriter;

/**
 *	情報登録/認証促進 BATCH
 *  アカウント保有＆入会必須登録情報に未登録情報があるユーザーへプッシュ 
 *  "【ロジック判断】 ベース： 会員全員ー入会済ー退会 詳細条件： アカウント登録日時，入会情報登録状況（登録情報数、最後の登録日時），規定日数 "
 *  通知
 */

@Configuration
@EnableBatchProcessing
public class MemberUnBatchConfig {

    private JobBuilderFactory jobBuilderFactory;

    private StepBuilderFactory stepBuilderFactory;

    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    public MemberUnBatchConfig(JobBuilderFactory jobBuilderFactory,
                       StepBuilderFactory stepBuilderFactory,
                       SqlSessionFactory sqlSessionFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.sqlSessionFactory = sqlSessionFactory;
    }
	//非会员 读入数据
	 @Bean
    public MyBatisCursorItemReader<Account> Reader() {
        final MyBatisCursorItemReader<Account> reader = new MyBatisCursorItemReader<>();
        reader.setSqlSessionFactory(sqlSessionFactory);
        //Account表中，リクエスト回答時刻，为空的结果集
        reader.setQueryId("com.example.batch.repository.AccountMapper.selectList");
        return reader;
    }
    
		//非会员 有优惠券 读入数据
	 @Bean
    public MyBatisCursorItemReader<Account> CouponReader() {
        final MyBatisCursorItemReader<Account> reader = new MyBatisCursorItemReader<>();
        reader.setSqlSessionFactory(sqlSessionFactory);
        //Account表中，リクエスト回答時刻，为空的结果集
        reader.setQueryId("com.example.batch.repository.AccountMapper.selectUnmembeCouponList");
        return reader;
    } 
	 
 	//非会员处理数据
    @Bean
    public UnMemberProcessor processor() {
        return new UnMemberProcessor();
    }
    
 	//非会员 有优惠券处理数据
    @Bean
    public UnMemCouponProcessor CouponProcessor() {
        return new UnMemCouponProcessor();
    }
    //非会员写入数据
    @Bean
    public MessageWriter writer() {
        return new MessageWriter();
    }
    //非会员 有优惠券 写入数据
    @Bean
    public MyBatisBatchItemWriter<Message> CouponWriter() {
        final MyBatisBatchItemWriter<Message> writer = new MyBatisBatchItemWriter<>();
        writer.setSqlSessionFactory(sqlSessionFactory);
      //message表，向符合条件的用户双方发送message，向message表中插入要发送的message
        writer.setStatementId("com.example.batch.repository.MessageMapper.insert");
        return writer;
    }


  //监听
    @Bean
    public JobExecutionListener listener() {
        return new JobExecutionListener() {
        	private long startTime;
            private long endTime;
            @Override
            public void beforeJob(JobExecution jobExecution) {
            	//job开始时间
            	startTime = System.currentTimeMillis();
                System.out.println("before job");
            }

            @Override
            public void afterJob(JobExecution jobExecution) {
            	//job结束时间
            	endTime = System.currentTimeMillis();
                System.out.println("after job");
                //合计出此job的耗时
                System.out.println("耗时："+(endTime-startTime)+"ms");
            }
        };
    }

    //步骤：非会员
    @Bean
    public Step readEmpAndWriteBonus() {
        return stepBuilderFactory
                .get("readEmpAndWriteBonus")
                .<Account, Message>chunk(10)
                .reader(Reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

    //步骤：非会员 优惠券
	   @Bean
	    public Step readCouponWrite() {
	        return stepBuilderFactory
	                .get("readCouponWrite")
	                .<Account, Message>chunk(10)
	                .reader(CouponReader())
	                .processor(CouponProcessor())
	                .writer(CouponWriter())
	                .build();
	    }

    
    //执行job
    @Bean
    public Job singleJob() {
        return jobBuilderFactory
                .get("unmem")
                .incrementer(new RunIdIncrementer())
                .listener(listener())
                .flow(readEmpAndWriteBonus())
                .next(readCouponWrite())
                .end()
                .build();
    }

}
