package com.example.batch.config;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;

import com.example.batch.BespeakTime1H.CBespeakTime1HProcessor;
import com.example.batch.BespeakTime1H.LBespeakTime1HProcessor;
import com.example.batch.domain.Loan;
import com.example.batch.domain.Message;
import com.example.batch.domain.Share;

/**
*	65,66处理貸出BATCH
*	BATCH
* 	貸出関連
*  シェア前チェック促進
*  「貸出確定」ユーザーへ予約開始時間の1時間前にプッシュ
*  確認事項トップ
*  通知専用
*/

@Configuration
@EnableBatchProcessing
public class BespeakTime1HBatchConfig {
	  private JobBuilderFactory jobBuilderFactory;

	    private StepBuilderFactory stepBuilderFactory;

	    private SqlSessionFactory sqlSessionFactory;

	    @Autowired
	    public BespeakTime1HBatchConfig(JobBuilderFactory jobBuilderFactory,
	                       StepBuilderFactory stepBuilderFactory,
	                       SqlSessionFactory sqlSessionFactory) {
	        this.jobBuilderFactory = jobBuilderFactory;
	        this.stepBuilderFactory = stepBuilderFactory;
	        this.sqlSessionFactory = sqlSessionFactory;
	    }
	//读入数据 收件人 预约者
	 @Bean
	    public MyBatisCursorItemReader<Share> reader() {
	        final MyBatisCursorItemReader<Share> reader = new MyBatisCursorItemReader<>();
	        reader.setSqlSessionFactory(sqlSessionFactory);
	        //シェア，share表中，状态值为“ 1 ”的结果集 ,测试暂定为1
	        
	        reader.setQueryId("com.example.batch.repository.ShareMapper.selectListStart1H");
	        return reader;
	    }
	//读入数据 收件人 车主 
	 @Bean
	    public MyBatisCursorItemReader<Loan> memberReader() {
	        final MyBatisCursorItemReader<Loan> reader = new MyBatisCursorItemReader<>();
	        reader.setSqlSessionFactory(sqlSessionFactory);
	      //シェア，share表中，状态值为“ 1 ”的结果集 ,测试暂定为1
	        reader.setQueryId("com.example.batch.repository.LoanMapper.LselectListStart1H");
	        return reader;
	    }


	 	//处理数据 收件人 预约者
	    @Bean
	    public LBespeakTime1HProcessor processor() {
	        return new LBespeakTime1HProcessor();
	    }
	 	//处理数据 收件人 车主
	    @Bean
	    public CBespeakTime1HProcessor carprocessor() {
	        return new CBespeakTime1HProcessor();
	    }

	    
	    //写入数据  收件人 预约者
	    @Bean
	    public MyBatisBatchItemWriter<Message> writer() {
	        final MyBatisBatchItemWriter<Message> writer = new MyBatisBatchItemWriter<>();
	        writer.setSqlSessionFactory(sqlSessionFactory);
	      //message表，向符合条件的用户双方发送message，向message表中插入要发送的message
	        writer.setStatementId("com.example.batch.repository.MessageMapper.insert");
	        return writer;
	    }

	    
	    //写入数据  收件人 车主
	    @Bean
	    public MyBatisBatchItemWriter<Message> memberWriter() {
	        final MyBatisBatchItemWriter<Message> writer = new MyBatisBatchItemWriter<>();
	        writer.setSqlSessionFactory(sqlSessionFactory);
	      //message表，向符合条件的用户双方发送message，向message表中插入要发送的message
	        writer.setStatementId("com.example.batch.repository.MessageMapper.insert");
	        return writer;
	    }

	//监听
	    @Bean
	    public JobExecutionListener listener() {
	        return new JobExecutionListener() {
	        	private long startTime;
	            private long endTime;
	            @Override
	            public void beforeJob(JobExecution jobExecution) {
	            	startTime = System.currentTimeMillis();
	                System.out.println("before job");
	            }

	            @Override
	            public void afterJob(JobExecution jobExecution) {
	            	endTime = System.currentTimeMillis();
	                System.out.println("after job");
	                System.out.println("耗时："+(endTime-startTime)+"ms");
	            }
	        };
	    }
	  //步骤：给预约者发message
	    @Bean
	    public Step readShareAndWriteMessage() {
	        return stepBuilderFactory
	                .get("readShareAndWriteMessage")
	                .<Share, Message>chunk(10)
	                .reader(reader())
	                .processor(processor())
	                .writer(writer())
	                .build();
	    }
	//步骤：给车主发message
	   @Bean
	    public Step readmemberWrite() {
	        return stepBuilderFactory
	                .get("readmemberWrite")
	                .<Loan, Message>chunk(10)
	                .reader(memberReader())
	                .processor(carprocessor())
	                .writer(memberWriter())
	                .build();
	    }
	//Batch   JOB
	    @Bean
	    public Job singleJob() {
	        return jobBuilderFactory
	                .get("BespeakTime1H")
	                .incrementer(new RunIdIncrementer())
	                .listener(listener())
	                .flow(readShareAndWriteMessage())
	                .next(readmemberWrite())
	                .end()
	                .build();
	    }

	}
