package com.example.batch.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import com.example.batch.domain.Member;
import com.example.batch.domain.Message;
import com.example.batch.membernew1m.MemberNew1MProcessor;

/**
 *	前借促進 BATCH
 *  "入会一定期間（１カ月）後に、一度もシェアしてない会員に、 前借での予約を促進"
 *  "【ロジック判断】 ベース： 会員全員×入会済×入会日×シェア全件チェック 詳細条件： 入会日，シェアレコードなし，規定日数 "
 *  通知
 */

//@Configuration
//@EnableBatchProcessing
public class MemberNew1MBatch {

    private JobBuilderFactory jobBuilderFactory;

    private StepBuilderFactory stepBuilderFactory;

    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    public MemberNew1MBatch(JobBuilderFactory jobBuilderFactory,
                       StepBuilderFactory stepBuilderFactory,
                       SqlSessionFactory sqlSessionFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.sqlSessionFactory = sqlSessionFactory;
    }
//读入数据
 @Bean
    public MyBatisCursorItemReader<Member> Reader() {
        final MyBatisCursorItemReader<Member> reader = new MyBatisCursorItemReader<>();
        reader.setSqlSessionFactory(sqlSessionFactory);
      //会员入会后，1个月内，没有共享记录，结果集
        reader.setQueryId("com.example.batch.repository.MemberMapper.selectUnShare");
        return reader;
    }
    
 	//处理数据
    @Bean
    public MemberNew1MProcessor processor() {
        return new MemberNew1MProcessor();
    }
    //写入数据
    @Bean
    public MyBatisBatchItemWriter<Message> writer() {
        final MyBatisBatchItemWriter<Message> writer = new MyBatisBatchItemWriter<>();
        writer.setSqlSessionFactory(sqlSessionFactory);
      //message表，向符合条件的用户双方发送message，向message表中插入要发送的message
        writer.setStatementId("com.example.batch.repository.MessageMapper.insert");
        return writer;
    }



  //监听
    @Bean
    public JobExecutionListener listener() {
        return new JobExecutionListener() {
        	private long startTime;
            private long endTime;
            @Override
            public void beforeJob(JobExecution jobExecution) {
            	//job开始时间
            	startTime = System.currentTimeMillis();
                System.out.println("before job");
            }

            @Override
            public void afterJob(JobExecution jobExecution) {
            	//job结束时间
            	endTime = System.currentTimeMillis();
                System.out.println("after job");
                //合计出此job的耗时
                System.out.println("耗时："+(endTime-startTime)+"ms");
            }
        };
    }

    //步骤：
    @Bean
    public Step readEmpAndWriteBonus() {
        return stepBuilderFactory
                .get("readEmpAndWriteBonus")
                .<Member, Message>chunk(10)
                .reader(Reader())
                .processor(processor())
                .writer(writer())
                .build();
    }


    
    //执行job
    @Bean
    public Job singleJob() {
        return jobBuilderFactory
                .get("NewMem1M")
                .incrementer(new RunIdIncrementer())
                .listener(listener())
                .flow(readEmpAndWriteBonus())
                .end()
                .build();
    }

}