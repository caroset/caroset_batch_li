package com.example.batch.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.mybatis.spring.batch.MyBatisCursorItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
//import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.context.annotation.Bean;
//import org.springframework.core.io.FileSystemResource;
import com.example.batch.chunk.BonusCalcProcessor;
import com.example.batch.domain.Loan;
import com.example.batch.domain.Message;

/**
 *	60，处理貸出BATCH
 * 	貸出関連
 *  予約リクエスト返信リマインド  
 *  予約リクエストを受信して期限まで3時間切ってリアクションをしていない際にプッシュ
 *  通知専用
 */

//@Configuration
//@EnableBatchProcessing
public class BatchConfig {

    private JobBuilderFactory jobBuilderFactory;

    private StepBuilderFactory stepBuilderFactory;

    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    public BatchConfig(JobBuilderFactory jobBuilderFactory,
                       StepBuilderFactory stepBuilderFactory,
                       SqlSessionFactory sqlSessionFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.sqlSessionFactory = sqlSessionFactory;
    }
//读入数据
 @Bean
    public MyBatisCursorItemReader<Loan> Reader() {
        final MyBatisCursorItemReader<Loan> reader = new MyBatisCursorItemReader<>();
        reader.setSqlSessionFactory(sqlSessionFactory);
        //loan表中，リクエスト回答時刻，为空的结果集
        reader.setQueryId("com.example.batch.repository.LoanMapper.selectUnllEndData");
        return reader;
    }
    
/*    @Bean
    public MyBatisCursorItemReader memberReader() {
        final MyBatisCursorItemReader<Emp> reader = new MyBatisCursorItemReader<>();
        reader.setSqlSessionFactory(sqlSessionFactory);
        reader.setQueryId("com.example.batch.repository.EmpMapper.findAll");
        return reader;
    }*/

/*    @Bean
    public MyBatisCursorItemReader bonusReader() {
        final MyBatisCursorItemReader<Bonus> reader = new MyBatisCursorItemReader<>();
        reader.setSqlSessionFactory(sqlSessionFactory);
        reader.setQueryId("com.example.batch.repository.BonusMapper.findAll");
        return reader;
    }*/
 	//处理数据
    @Bean
    public BonusCalcProcessor processor() {
        return new BonusCalcProcessor();
    }
    //写入数据
    @Bean
    public MyBatisBatchItemWriter<Message> writer() {
        final MyBatisBatchItemWriter<Message> writer = new MyBatisBatchItemWriter<>();
        writer.setSqlSessionFactory(sqlSessionFactory);
      //message表，向符合条件的用户双方发送message，向message表中插入要发送的message
        writer.setStatementId("com.example.batch.repository.MessageMapper.insert");
        return writer;
    }

   /* @Bean
    public FlatFileItemWriter<Bonus> fileWriter() {
        final FlatFileItemWriter<Bonus> writer = new FlatFileItemWriter<>();
        writer.setResource(new FileSystemResource("out/bonus.txt"));
        writer.setHeaderCallback(headerWriter -> headerWriter.append("EMP_ID")
                .append(",")
                .append("PAYMENTS"));
        writer.setLineAggregator(item -> new StringBuilder()
                .append(item.getEmpId())
                .append(",")
                .append(item.getPayments())
                .toString());
        return writer;
    }*/

  //监听
    @Bean
    public JobExecutionListener listener() {
        return new JobExecutionListener() {
        	private long startTime;
            private long endTime;
            @Override
            public void beforeJob(JobExecution jobExecution) {
            	//job开始时间
            	startTime = System.currentTimeMillis();
                System.out.println("before job");
            }

            @Override
            public void afterJob(JobExecution jobExecution) {
            	//job结束时间
            	endTime = System.currentTimeMillis();
                System.out.println("after job");
                //合计出此job的耗时
                System.out.println("耗时："+(endTime-startTime)+"ms");
            }
        };
    }

    //步骤：
    @Bean
    public Step readEmpAndWriteBonus() {
        return stepBuilderFactory
                .get("readEmpAndWriteBonus")
                .<Loan, Message>chunk(10)
                .reader(Reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

/*    @Bean
    public Step readBonusAndWriteFile() {
        return stepBuilderFactory
                .get("readBonusAndWriteFile")
                .<Message, Bonus>chunk(10)
                .reader(bonusReader())
                .writer(fileWriter())
                .build();
    }
*/
    
    //执行job
    @Bean
    public Job singleJob() {
        return jobBuilderFactory
                .get("singleJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener())
                .flow(readEmpAndWriteBonus())
                .end()
                .build();
    }

   /* @Bean
    public Job multiJob() {
        return jobBuilderFactory
                .get("multiJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener())
                .flow(readEmpAndWriteBonus())
                .next(readBonusAndWriteFile())
                .end()
                .build();
    }*/
}
