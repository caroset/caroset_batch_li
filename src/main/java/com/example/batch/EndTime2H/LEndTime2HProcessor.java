package com.example.batch.EndTime2H;

import org.springframework.batch.item.ItemProcessor;

import com.example.batch.domain.Message;
import com.example.batch.domain.Share;

public class LEndTime2HProcessor implements ItemProcessor<Share, Message> {

    @Override
    public Message process(Share share) throws Exception {
    	  final Message message = new Message();
    	// 判断参数集合是否为空
    	if (share == null) {
    		//不符合条件的，舍弃返回null
	    	 return null;
/*//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");*/
        }else {
        	//在条件范围内，则给message赋值，之后根据message设计再进行修改
    	 	//会员id
        	 message.setMemberId(share.getMemberId());
        	 //借出id
             message.setLoanId(share.getCarId());
             //发送消息内容
             message.setContent("S马上发message,返却時間の2時間前に返却と返却前の給油等");
          //返回message
         return message;
        }
        
    }
    
}