package com.example.batch.EndTime2H;

import org.springframework.batch.item.ItemProcessor;
import com.example.batch.domain.Loan;
import com.example.batch.domain.Message;

public class CEndTime2HProcessor implements ItemProcessor<Loan, Message> {
	
    @Override
    public Message process(Loan loan) throws Exception {
	     //声明message
	    final Message message = new Message();
    	// 判断参数集合是否为空
    	if (loan == null) {
    		//不符合条件的，舍弃返回null
	    	 return null;
/*//异常，为空抛出为空信息
            throw new IllegalArgumentException("loan must not be null.");*/
        }else {
        	//在条件范围内，则给message赋值，之后根据message设计再进行修改
    	 	
	    	 //这里id反写，等message相关内容建好后更改
	    	 
	    	 //会员id
	        	 message.setMemberId(loan.getMemberId());
	        	 //借出id
	             message.setLoanId(loan.getLoanId());
	             //发送消息内容
	             message.setContent("L马上发message,返却時間の2時間前に返却と返却前の給油等");
	          //返回message
	         return message;
        }
        
    }
    
}
