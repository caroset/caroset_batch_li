package com.example.batch;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@MapperScan("com.example.batch.repository")
public class BatchExampleApplication {

	public static void main(String[] args) {
		System.setProperty("spring.batch.job.names","AMPMtime");
		SpringApplication.run(BatchExampleApplication.class, args);
	}
/*	 public static void main(String[] args) {
		    SpringApplication.run(BatchExampleApplication.class, args);
		  }*/
}
