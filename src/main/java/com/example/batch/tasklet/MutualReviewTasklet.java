package com.example.batch.tasklet;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Loan;
import com.example.batch.domain.Share;

/**
 * ステータスシェア開始（レビュー期限締切り）
 * @Description 双方都有评价，并在2天之内，返回结果集， 初始状态分区1
 * 状態区分＝ レビュー登録待ち -->過去のドライブ
 * @author Li
 */
@Component
public class MutualReviewTasklet implements Tasklet{
	private static final Logger log = LoggerFactory.getLogger(OcarborrowTasklet.class);
	
	@Autowired
	ShareService shareService;
	
	@Autowired
	LoanService loanService;
	
	//对参数时间进行处理，取得当前时间-2天
	public Timestamp getTime() {
		
		Timestamp tt = new Timestamp(System.currentTimeMillis());
		  Calendar cc = Calendar.getInstance();
		  cc.setTime(tt);
		  //当前天数-14
		  cc.add(Calendar.DAY_OF_MONTH, -14);

		  return new Timestamp(cc.getTimeInMillis());
	}
	
	 @Override
	    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		//对参数时间进行处理，取得当前时间-2天
		 Timestamp day1 = getTime() ;
		 
		 //シェア
		 //レビュー期限締切り，終了時間14天之后，返回结果list 状态分区1 レビュー登録待ち-> 2過去のドライブ
	    	List<Share> list = shareService.selectEndTime(day1);
	    	if(list != null) {	
	    		//循环list，并设定状态值（22暂定）
	    	for (Share share:list) {
	    		share.setState(22);
	    		//更新状态
	    		shareService.updateByPrimaryKeySelective(share);}
	    	}else {
	    		//list为空，共通设定时处理
	    		System.out.println("当前没有需要处理的数据");
	    	}
	    	
	    	//貸出
	    	//レビュー期限締切り，終了時間14天之后，返回结果list 状态分区1レビュー登録待ち -> 2 過去の貸出
	    	List<Loan> listSee = loanService.selectEndTime(day1);
	    	if(listSee != null) {	
	    		//循环list，并设定状态值（33暂定）
	    	for (Loan loan:listSee) {
	    		loan.setStateDivision(33);
	    		//更新状态
	    		loanService.updateByPrimaryKeySelective(loan);}
	    	}else {
//list为空，共通设定时处理		
	    		System.out.println("当前没有需要处理的数据");
	    	}
	    	//步骤完成
	    	  return RepeatStatus.FINISHED;	
	    }

}
