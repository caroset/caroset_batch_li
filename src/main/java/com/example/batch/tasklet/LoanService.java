package com.example.batch.tasklet;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Loan;
import com.example.batch.domain.Share;
import com.example.batch.repository.LoanMapper;

@Service
public class LoanService {
	
	@Autowired
	private LoanMapper loanMapper;
	/**
	 * ステータスシェア開始（予約リクエスト承認期限切れ） to  貸出し
	 * @Description 检查预约时间，检索出预约日期大于当前系统时间1天的结果list 状态分区1
	 * @param Loan day1
	 */
	public List<Loan> selectBespeakTime(java.sql.Timestamp day1){

		return loanMapper.selectBespeakTime(day1);
	}
	/**
	 * @Description レビュー期限締切り，終了時間14天之后，返回结果list to  貸出し
	 * @param Loan day1  系统时间-14天
	 */
	public List<Loan> selectEndTime(java.sql.Timestamp day1){

		return loanMapper.selectEndTime(day1);
	}

	/**
	 * @Description 检查開始時間，检索開始時間 小于等于 当前系统时间的结果list 状态分区2貸出し予定
	 * 
	 * @param Loan 系统当前时间
	 */
	public List<Loan> selectScarborrow(Date date){

		return loanMapper.selectScarborrow(date);
	}
	
	/**
	 * ステータスシェア開始（シェア終了）
	 * @Description 检查終了時間，检索終了時間  小于等于 当前系统时间结果list  状态分区3
	 * @param Loan 系统时间
	 */
	public List<Loan> selectOcarborrow(Date date){

		return loanMapper.selectOcarborrow(date);
	}
	
	
	/**
	 * ステータスシェア開始（問い合わせ期限切れ）
	 * @Description 检查送信日時，检索出送信日時大于当前系统时间3天的结果list
	 * @param Share 当前系统时间-3天，状態区分=問い合わせ中(1)-->過去の問い合わせ(99)
	 */
	public List<Loan> selectConsultMessage(java.sql.Timestamp day1){

		return loanMapper.selectConsultMessage(day1);
	}
	/**
	 * @Description 更新状态 状态分区
	 * @param Loan StateDivision
	 */
	public int updateByPrimaryKeySelective(Loan loan) {

		return loanMapper.updateByPrimaryKeySelective(loan);
	}
}
