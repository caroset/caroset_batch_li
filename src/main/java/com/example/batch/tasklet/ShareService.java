package com.example.batch.tasklet;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Share;
import com.example.batch.repository.ShareMapper;

@Service
public class ShareService {
	@Autowired
	private ShareMapper shareMapper;

	/**
	 * @Description 繝｢繝�繝ｫ縺ｫ繧医ｋ縺ｨ 繧｢繧ｫ繧ｦ繝ｳ繝医�ｮ荳�莉ｶ繝�繝ｼ繧ｿ繧呈諺蜈･縺励∪縺励◆
	 * @param member 莨壼藤
	 */
	public void insert(Share share) {
		shareMapper.insert(share);
	}

	/**
	 * ステータスシェア開始（予約リクエスト承認期限切れ）
	 * @Description 检查预约时间，检索出预约日期大于当前系统时间1天的结果list 状态分区1
	 * @param Share bespeak_time
	 */
	public List<Share> selectBespeakTime(java.sql.Timestamp day1){

		return shareMapper.selectBespeakTime(day1);
	}
	/**
	 * @Description レビュー期限締切り，終了時間14天之后，返回结果list
	 * @param Share end_time 系统时间-14天
	 */
	public List<Share> selectEndTime(java.sql.Timestamp day1){

		return shareMapper.selectEndTime(day1);
	}
	
	/**
	 * @Description 检查開始時間，检索開始時間 小于等于 当前系统时间的结果list 状态分区2ドライブ予定
	 * 
	 * @param Share bespeak_time
	 */
	public List<Share> selectScarborrow(Date date){

		return shareMapper.selectScarborrow(date);
	}
	/**
	 * ステータスシェア開始（シェア終了）
	 * @Description 检查終了時間，检索終了時間  小于等于 当前系统时间结果list  状态分区3
	 * @param Share end_date
	 */
	public List<Share> selectOcarborrow(Date date){

		return shareMapper.selectOcarborrow(date);
	}
	/**
	 * ステータスシェア開始（問い合わせ期限切れ）
	 * @Description 检查送信日時，检索出送信日時大于当前系统时间3天的结果list
	 * @param Share 当前系统时间-3天，状態区分=問い合わせ中(1)-->過去の問い合わせ(99)
	 */
	public List<Share> selectConsultMessage(java.sql.Timestamp day1){

		return shareMapper.selectConsultMessage(day1);
	}
	
	public Share selectByPrimaryKey(Share share) {

		return shareMapper.selectByPrimaryKey(share);
	}

	/**
	 * @Description 荳ｻ繧ｭ繝ｼ縺ｫ繧医ｋ縺ｨ 繧｢繧ｫ繧ｦ繝ｳ繝医�ｮ縺�縺上▽縺矩��逶ｮ縺ｮ繝�繝ｼ繧ｿ繧呈峩譁ｰ縺励∪縺励◆
	 * @param member 莨壼藤
	 */
	public int updateByPrimaryKeySelective(Share share) {

		return shareMapper.updateByPrimaryKeySelective(share);
	}

}
