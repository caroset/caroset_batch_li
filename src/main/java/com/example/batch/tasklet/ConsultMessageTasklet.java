package com.example.batch.tasklet;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Loan;
import com.example.batch.domain.Share;

/**
 * ステータスシェア開始（問い合わせ期限切れ）to シェア  to 貸出し
 * @Description 检查发信时间，对预约日期大于当前系统时间3天的结果集，更新状态分区1（暂定）->99
 *  @author Li
 */
@Component
public class ConsultMessageTasklet  implements Tasklet {

	private static final Logger log = LoggerFactory.getLogger(ShareStartTasklet.class);
	
	@Autowired
	ShareService shareService;
	
	@Autowired
	LoanService loanService;
	//对参数时间进行处理，当前时间-3天
	public Timestamp getTime() {
		
		Timestamp tt = new Timestamp(System.currentTimeMillis());
		  Calendar cc = Calendar.getInstance();
		  cc.setTime(tt);
		  //当前天数-1
		  cc.add(Calendar.DAY_OF_MONTH, -3);

		  return new Timestamp(cc.getTimeInMillis());
	}
	
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
    	//取得当前时间-3天的时间
    	Timestamp day1 = getTime() ;
    	
    	//to シェア
    	//检查送信日時，检索message送信日時大于当前系统时间3天的结果list 
    	List<Share> list = shareService.selectConsultMessage(day1);
    	//判断list是否为空
    	if (list != null) {
    		//循环list，并设定状态值（99暂定）.状態区分=問い合わせ中(1)-->過去の問い合わせ(99)
    		for (Share share:list) {
        		share.setState(99);
        		//更新状态
        		shareService.updateByPrimaryKeySelective(share);
        	}
    	}else {
//list为空，共通设定时处理
    		System.out.println("当前没有需要处理的数据");
    	}
    	
    	//to 貸出し
    	//检查送信日時，检索message送信日時大于当前系统时间3天的结果list 状態区分=問い合わせ中(1)-->過去の問い合わせ(99)
    	List<Loan> listLoan = loanService.selectConsultMessage(day1);
    	//判断list是否为空
    	if (listLoan != null) {
    		//循环list，并设定状态值（99暂定）.状態区分=問い合わせ中(1)-->過去の問い合わせ(99)
    		for (Loan loan:listLoan) {
    			loan.setStateDivision(99);
        		//更新状态
        		loanService.updateByPrimaryKeySelective(loan);
        	}
    	}else {
//list为空，共通设定时处理
    		System.out.println("当前没有需要处理的数据");
    	}	
    	//步骤完成
    	  return RepeatStatus.FINISHED;
    	
    }

}
