package com.example.batch.tasklet;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Review;
/**
 * @author Li
 * @Description 双方都有评价，并在2天之内，返回结果集， 状态分区由1 -> 2 双方公开评论
 */
@Component
public class ReviewTermTasklet implements Tasklet{
	private static final Logger log = LoggerFactory.getLogger(OcarborrowTasklet.class);
	
	@Autowired
	ReviewService reviewService;
	
	//对参数时间进行处理，取得当前时间-2天
	public Timestamp getTime() {
		
		Timestamp tt = new Timestamp(System.currentTimeMillis());
		  Calendar cc = Calendar.getInstance();
		  cc.setTime(tt);
		  //当前天数-2
		  cc.add(Calendar.DAY_OF_MONTH, -2);

		  return new Timestamp(cc.getTimeInMillis());
	}
	
	 @Override
	    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		//对参数时间进行处理，取得当前时间-2天
		 Timestamp day1 = getTime() ;
		 
		 //规定时间内，双方都评论了，则双方都可以看到
		 //检查评论，双方都有评价，并在2天之内，返回结果list 状态分区1 -> 2 双方公开评论
	    	List<Review> list = reviewService.selectInsertAll(day1);
	    	if(list != null) {	
	    		//循环list，并设定状态值（22暂定）
	    	for (Review review:list) {
	    		review.setReviewState(22);
	    		//更新状态
	    		reviewService.updateByPrimaryKeySelective(review);}
	    	}else {
	    		//list为空，共通设定时处理
	    		System.out.println("当前没有需要处理的数据");
	    	}
	    	
	    	//规定时间以后，双方都能看到评论
	    	//检查评论，评价时间2天以后，返回结果list 状态分区1 -> 2 双方公开评论
	    	List<Review> listSee = reviewService.selectInsertTimee2(day1);
	    	if(listSee != null) {	
	    		//循环list，并设定状态值（33暂定）
	    	for (Review review:listSee) {
	    		review.setReviewState(33);
	    		//更新状态
	    		reviewService.updateByPrimaryKeySelective(review);}
	    	}else {
//list为空，共通设定时处理		
	    		System.out.println("当前没有需要处理的数据");
	    	}
	    	//步骤完成
	    	  return RepeatStatus.FINISHED;	
	    }
}
