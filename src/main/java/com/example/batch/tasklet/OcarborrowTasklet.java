package com.example.batch.tasklet;

import java.sql.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Loan;
import com.example.batch.domain.Share;
/**
 * ステータスシェア開始（シェア終了）
 * @author Li
 * @Description 检查終了時間，終了時間  小于等于 当前系统时间结果集， 状态分区3->77
 */

@Component
public class OcarborrowTasklet implements Tasklet{
private static final Logger log = LoggerFactory.getLogger(OcarborrowTasklet.class);
	
	@Autowired
	ShareService shareService;
	
	@Autowired
	LoanService loanService;
	
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
    	//取得当前时间
    	Date nowDate = new Date(System.currentTimeMillis());
    	
    	//to シェア
    	//检查終了時間，检索終了時間  小于等于 当前系统时间结果list  状態区分 = ドライブ中(3暂定) -->レビュー登録待ち（77暂定）
    	List<Share> list = shareService.selectOcarborrow(nowDate);
    	if(list != null) {
    		//循环list，状態区分 = ドライブ中(3暂定) -->レビュー登録待ち（77暂定）
    	for (Share share:list) {
    		share.setState(77);
    		//更新状态
    		shareService.updateByPrimaryKeySelective(share);	}
    	}else {
//list为空，共通设定时处理
    		System.out.println("当前没有需要处理的数据");
    	}
    	
    	//to 貸出し
    	//检查終了時間，检索終了時間  小于等于 当前系统时间结果list 
    	List<Loan> listLoan = loanService.selectScarborrow(nowDate);
    	if(listLoan != null) {
    		//循环list，并设定状态值（77暂定）
        	for (Loan loan:listLoan) {
        		//状態区分 = ドライブ中(3暂定) -->レビュー登録待ち（77暂定）
        		loan.setStateDivision(77);
        		//更新状态
        		loanService.updateByPrimaryKeySelective(loan);
        	}
    	}else {
//list为空，共通设定时处理    		
    		System.out.println("当前没有需要处理的数据");
    	}
    	//步骤完成
    	  return RepeatStatus.FINISHED;
    	
    }

}
