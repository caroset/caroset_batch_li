package com.example.batch.tasklet;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Message;

@Component
public class MessageTasklet implements Tasklet {
	@Autowired
	MessageService messageService;

	//取得当前时间-1天
		public Timestamp getTime() {
		
		Timestamp tt = new Timestamp(System.currentTimeMillis());
		  Calendar cc = Calendar.getInstance();
		  cc.setTime(tt);
		  //当前天数-1
		  cc.add(Calendar.DAY_OF_MONTH, -1);
		  
		  return new Timestamp(cc.getTimeInMillis());
		}
	
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
    	////取得当前时间-1天
    	Timestamp day1 = getTime() ;
    	
    	List<Message> list = messageService.selectMessage24(day1);
    	if(list != null) {
    	for (Message message:list) {
    		
    		message.setType(66);
    		//更新状态
    		messageService.updateByPrimaryKeySelective(message);	}
    	}else {
    		//测试用
    		System.out.println("当前没有需要处理的数据");
    	}
    	  return RepeatStatus.FINISHED;  	
    }

}
