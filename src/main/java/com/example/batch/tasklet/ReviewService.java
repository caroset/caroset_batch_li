package com.example.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Review;
import com.example.batch.repository.ReviewMapper;

@Service
public class ReviewService {
	@Autowired
	private ReviewMapper reviewMapper;

	/**
	 * @Description 插入新数据
	 * @param Review 
	 */
	public void insert(Review review) {
		reviewMapper.insert(review);
	}

	/**
	 * @Description 检查评论，双方都有评价，并在2天之内，返回结果list 状态分区1 -> 2 双方公开评论
	 * @param Review insert_time 目前暂定此时间
	 */
	public List<Review> selectInsertAll(java.sql.Timestamp day1){

		return reviewMapper.selectInsertAll(day1);
	}
	/**
	 * @Description 检查评论，评价时间2天以后，返回结果list 状态分区1 -> 2 双方公开评论
	 * @param Review
	 */
	public List<Review> selectInsertTimee2(java.sql.Timestamp day1){

		return reviewMapper.selectInsertTimee2(day1);
	}
	
	/**
	 * @Description 根据id检索评论信息
	 * @param Review
	 */
	public Review selectByPrimaryKey(int id) {

		return reviewMapper.selectByPrimaryKey(id);
	}

	/**
	 * @Description 更新数据
	 * @param Review
	 */
	public int updateByPrimaryKeySelective(Review review) {

		return reviewMapper.updateByPrimaryKeySelective(review);
	}


}
