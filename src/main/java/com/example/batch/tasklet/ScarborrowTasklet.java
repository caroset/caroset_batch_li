package com.example.batch.tasklet;

import java.sql.Date;
//import java.sql.Timestamp;
//import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.batch.domain.Loan;
import com.example.batch.domain.Share;

/**
 * ステータスシェア開始（ドライブ中）  to シェア  to 貸出し
 * @author Li
 * @Description 检查開始時間，对開始時間 小于等于 当前系统时间的结果集，更新状态分区2（暂定）
 */

@Component
public class ScarborrowTasklet implements Tasklet  {
private static final Logger log = LoggerFactory.getLogger(ScarborrowTasklet.class);
	
	@Autowired
	ShareService shareService;
	@Autowired
	LoanService loanService;
	
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
    	//当前系统时间
    	Date nowDate = new Date(System.currentTimeMillis());
    	
    	//to シェア
    	//检查開始時間，检索開始時間 小于等于 当前系统时间的结果list
    	List<Share> list = shareService.selectScarborrow(nowDate);
    	if(list != null) {
    		//循环list，并设定状态值（88暂定）
        	for (Share share:list) {
        		//状態区分 :ドライブ予定(2暂定)-->ドライブ中（88暂定）
        		share.setState(88);
        		//更新状态
        		shareService.updateByPrimaryKeySelective(share);
        	}
    	}else {
//list为空，共通设定时处理    		
    		System.out.println("当前没有需要处理的数据");
    	}
    	
    	//to 貸出し
    	//检查開始時間，检索開始時間 小于等于 当前系统时间的结果list
    	List<Loan> listLoan = loanService.selectScarborrow(nowDate);
    	if(listLoan != null) {
    		//循环list，并设定状态值（88暂定）
        	for (Loan loan:listLoan) {
        		//状態区分 :ドライブ予定(2暂定)-->ドライブ中（88暂定）
        		loan.setStateDivision(88);
        		//更新状态
        		loanService.updateByPrimaryKeySelective(loan);
        	}
    	}else {
//list为空，共通设定时处理    		
    		System.out.println("当前没有需要处理的数据");
    	}
    	   	
    	//步骤完成
    	  return RepeatStatus.FINISHED;

    }

}
