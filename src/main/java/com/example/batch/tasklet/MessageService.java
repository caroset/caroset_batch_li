package com.example.batch.tasklet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.batch.domain.Message;
import com.example.batch.repository.MessageMapper;

@Service
public class MessageService {
	@Autowired
	private MessageMapper messageMapper;

	/**
	 * @Description 繝｢繝�繝ｫ縺ｫ繧医ｋ縺ｨ 繧｢繧ｫ繧ｦ繝ｳ繝医�ｮ荳�莉ｶ繝�繝ｼ繧ｿ繧呈諺蜈･縺励∪縺励◆
	 * @param member 莨壼藤
	 */
	public void insert(Message message) {
		messageMapper.insert(message);
	}

	/**
	 * @Description 荳ｻ繧ｭ繝ｼ縺ｫ繧医ｋ縺ｨ 繧｢繧ｫ繧ｦ繝ｳ繝医�ｮ荳�莉ｶ繝�繝ｼ繧ｿ繧呈､懃ｴ｢蜿門ｾ励＠縺ｾ縺励◆
	 * @param member 莨壼藤
	 */
	public List<Message> selectMessage24(java.sql.Timestamp day1) {

		return messageMapper.selectMessage24(day1);
	}

	/**
	 * @Description 荳ｻ繧ｭ繝ｼ縺ｫ繧医ｋ縺ｨ 繧｢繧ｫ繧ｦ繝ｳ繝医�ｮ縺�縺上▽縺矩��逶ｮ縺ｮ繝�繝ｼ繧ｿ繧呈峩譁ｰ縺励∪縺励◆
	 * @param member 莨壼藤
	 */
	public int updateByPrimaryKeySelective(Message message) {

		return messageMapper.updateByPrimaryKeySelective(message);
	}
}
